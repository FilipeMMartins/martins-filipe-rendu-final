--

-- Database: filipe_martins_1c_104
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists martins_filipe_entreprise_bd_104_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS martins_filipe_entreprise_bd_104_2020;

-- Utilisation de cette base de donnée

USE martins_filipe_entreprise_bd_104_2020;



SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 27 Juin 2020 à 18:37
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `martins_filipe_entreprise_bd_104_2020a`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_adresses`
--

CREATE TABLE `t_adresses` (
  `id_Adresse` int(11) NOT NULL,
  `RueAdresse` varchar(80) NOT NULL,
  `NpaAdresse` int(10) NOT NULL,
  `VilleAdresse` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_entreprise`
--

CREATE TABLE `t_entreprise` (
  `id_Entreprise` int(11) NOT NULL,
  `NomEntreprise` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_mails`
--

CREATE TABLE `t_mails` (
  `id_Mail` int(11) NOT NULL,
  `NomMail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `id_Personne` int(11) NOT NULL,
  `NomPers` varchar(50) NOT NULL,
  `PrenomPers` varchar(30) NOT NULL,
  `DateNaissPers` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_adresse`
--

CREATE TABLE `t_pers_adresse` (
  `id_pers_adresse` int(11) NOT NULL,
  `fk_pers` int(11) NOT NULL,
  `fk_adresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_mail`
--

CREATE TABLE `t_pers_mail` (
  `id_pers_mail` int(11) NOT NULL,
  `fk_pers` int(11) NOT NULL,
  `fk_mail` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_adresses`
--
ALTER TABLE `t_adresses`
  ADD PRIMARY KEY (`id_Adresse`);

--
-- Index pour la table `t_entreprise`
--
ALTER TABLE `t_entreprise`
  ADD PRIMARY KEY (`id_Entreprise`);

--
-- Index pour la table `t_mails`
--
ALTER TABLE `t_mails`
  ADD PRIMARY KEY (`id_Mail`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`id_Personne`);

--
-- Index pour la table `t_pers_adresse`
--
ALTER TABLE `t_pers_adresse`
  ADD PRIMARY KEY (`id_pers_adresse`),
  ADD KEY `fk_pers` (`fk_pers`),
  ADD KEY `fk_adresse` (`fk_adresse`);

--
-- Index pour la table `t_pers_mail`
--
ALTER TABLE `t_pers_mail`
  ADD PRIMARY KEY (`id_pers_mail`),
  ADD KEY `fk_pers` (`fk_pers`),
  ADD KEY `fk_mail` (`fk_mail`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_adresses`
--
ALTER TABLE `t_adresses`
  MODIFY `id_Adresse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT pour la table `t_entreprise`
--
ALTER TABLE `t_entreprise`
  MODIFY `id_Entreprise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `t_mails`
--
ALTER TABLE `t_mails`
  MODIFY `id_Mail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `id_Personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT pour la table `t_pers_adresse`
--
ALTER TABLE `t_pers_adresse`
  MODIFY `id_pers_adresse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `t_pers_mail`
--
ALTER TABLE `t_pers_mail`
  MODIFY `id_pers_mail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_pers_adresse`
--
ALTER TABLE `t_pers_adresse`
  ADD CONSTRAINT `t_pers_adresse_ibfk_1` FOREIGN KEY (`fk_pers`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_adresse_ibfk_2` FOREIGN KEY (`fk_adresse`) REFERENCES `t_adresses` (`id_Adresse`);

--
-- Contraintes pour la table `t_pers_mail`
--
ALTER TABLE `t_pers_mail`
  ADD CONSTRAINT `t_pers_mail_ibfk_1` FOREIGN KEY (`fk_pers`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_mail_ibfk_2` FOREIGN KEY (`fk_mail`) REFERENCES `t_mails` (`id_Mail`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
