# routes_gestion_pers_adresse.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les personnes et les adresses.

from flask import render_template, request, flash, session
from APP_ENTREPRISE import obj_mon_application
from APP_ENTREPRISE.ADRESSES.data_gestion_adresses import gestionadresses
from APP_ENTREPRISE.PERS_ADRESSE.data_gestion_pers_adresse import GestionPersAdresses


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /PERS_ADRESSE_afficher_concat
# Récupère la liste de tous les personnes et de tous les adresses associés aux personnes.
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/pers_adresse_afficher_concat/<int:id_Personne_sel>", methods=['GET', 'POST'])
def pers_adresse_afficher_concat (id_Personne_sel):
    print("id_Personne_sel ", id_Personne_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresses = GestionPersAdresses()
            # Récupère les données grâce à une requête MySql définie dans la classe gestionadresses()
            # Fichier data_gestion_adresses.py
            data_PERS_ADRESSE_afficher_concat = obj_actions_adresses.PERS_ADRESSE_afficher_data_concat(id_Personne_sel)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data adresses", data_PERS_ADRESSE_afficher_concat, "type ", type(data_PERS_ADRESSE_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_PERS_ADRESSE_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données adresses affichés dans PersAdresse!!", "success")
            else:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_PERS_ADRESSE" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("PERS_ADRESSE/PERS_ADRESSE_afficher.html",
                           data=data_PERS_ADRESSE_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_pers_adresse_selected
# Récupère la liste de tous les adresses du film sélectionné.
# Nécessaire pour afficher tous les "TAGS" des adresses, ainsi l'utilisateur voit les adresses à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_edit_pers_adresse_selected", methods=['GET', 'POST'])
def gf_edit_pers_adresse_selected ():
    if request.method == "GET":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresses = gestionadresses()
            # Récupère les données grâce à une requête MySql définie dans la classe gestionadresses()
            # Fichier data_gestion_adresses.py
            # Pour savoir si la table "t_adresses" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(PERS_ADRESSE_modifier_tags_dropbox.html)
            data_adresses_all = obj_actions_adresses.adresses_afficher_data()

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_adresses = GestionPersAdresses()

            # OM 2020.04.21 Récupère la valeur de "id_Personne" du formulaire html "PERS_ADRESSE_afficher.html"
            # l'utilisateur clique sur le lien "Modifier adresses de ce film" et on récupère la valeur de "id_Personne" grâce à la variable "id_Personne_adresses_edit_html"
            # <a href="{{ url_for('gf_edit_pers_adresse_selected', id_Personne_adresses_edit_html=row.id_Personne) }}">Modifier les adresses de ce film</a>
            id_Personne_adresses_edit = request.values['id_Personne_adresses_edit_html']

            # OM 2020.04.21 Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_Personne_adresses_edit'] = id_Personne_adresses_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_Personne_selected_dictionnaire = {"value_id_Personne_selected": id_Personne_adresses_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la classe GestionPersAdresses()
            # 1) Sélection du film choisi
            # 2) Sélection des adresses "déjà" attribués pour le film.
            # 3) Sélection des adresses "pas encore" attribués pour le film choisi.
            # Fichier data_gestion_pers_adresse.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "PERS_ADRESSE_afficher_data"
            data_pers_adresse_selected, data_PERS_ADRESSE_non_attribues, data_PERS_ADRESSE_attribues = \
                obj_actions_adresses.PERS_ADRESSE_afficher_data(valeur_id_Personne_selected_dictionnaire)

            lst_data_film_selected = [item['id_Personne'] for item in data_pers_adresse_selected]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_film_selected  ", lst_data_film_selected,
                  type(lst_data_film_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les adresses qui ne sont pas encore sélectionnés.
            lst_data_PERS_ADRESSE_non_attribues = [item['id_Adresse'] for item in data_PERS_ADRESSE_non_attribues]
            session['session_lst_data_PERS_ADRESSE_non_attribues'] = lst_data_PERS_ADRESSE_non_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_PERS_ADRESSE_non_attribues  ", lst_data_PERS_ADRESSE_non_attribues,
                  type(lst_data_PERS_ADRESSE_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les adresses qui sont déjà sélectionnés.
            lst_data_PERS_ADRESSE_old_attribues = [item['id_Adresse'] for item in data_PERS_ADRESSE_attribues]
            session['session_lst_data_PERS_ADRESSE_old_attribues'] = lst_data_PERS_ADRESSE_old_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_PERS_ADRESSE_old_attribues  ", lst_data_PERS_ADRESSE_old_attribues,
                  type(lst_data_PERS_ADRESSE_old_attribues))

            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data data_pers_adresse_selected", data_pers_adresse_selected, "type ", type(data_pers_adresse_selected))
            print(" data data_PERS_ADRESSE_non_attribues ", data_PERS_ADRESSE_non_attribues, "type ",
                  type(data_PERS_ADRESSE_non_attribues))
            print(" data_PERS_ADRESSE_attribues ", data_PERS_ADRESSE_attribues, "type ",
                  type(data_PERS_ADRESSE_attribues))

            # Extrait les valeurs contenues dans la table "t_adresses", colonne "RueAdresse"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_Adresse
            lst_data_PERS_ADRESSE_non_attribues = [item['RueAdresse'] for item in data_PERS_ADRESSE_non_attribues]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_all_adresses gf_edit_pers_adresse_selected ", lst_data_PERS_ADRESSE_non_attribues,
                  type(lst_data_PERS_ADRESSE_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_film_selected == [None]:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_PERS_ADRESSE" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données adresses affichées dans PersAdresse!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("PERS_ADRESSE/PERS_ADRESSE_modifier_tags_dropbox.html",
                           data_adresses=data_adresses_all,
                           data_film_selected=data_pers_adresse_selected,
                           data_adresses_attribues=data_PERS_ADRESSE_attribues,
                           data_adresses_non_attribues=data_PERS_ADRESSE_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_pers_adresse_selected
# Récupère la liste de tous les adresses du film sélectionné.
# Nécessaire pour afficher tous les "TAGS" des adresses, ainsi l'utilisateur voit les adresses à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_update_pers_adresse_selected", methods=['GET', 'POST'])
def gf_update_pers_adresse_selected ():
    if request.method == "POST":
        try:
            # Récupère l'id du film sélectionné
            id_Personne_selected = session['session_id_Personne_adresses_edit']
            print("session['session_id_Personne_adresses_edit'] ", session['session_id_Personne_adresses_edit'])

            # Récupère la liste des adresses qui ne sont pas associés au film sélectionné.
            old_lst_data_PERS_ADRESSE_non_attribues = session['session_lst_data_PERS_ADRESSE_non_attribues']
            print("old_lst_data_PERS_ADRESSE_non_attribues ", old_lst_data_PERS_ADRESSE_non_attribues)

            # Récupère la liste des adresses qui sont associés au film sélectionné.
            old_lst_data_PERS_ADRESSE_attribues = session['session_lst_data_PERS_ADRESSE_old_attribues']
            print("old_lst_data_PERS_ADRESSE_old_attribues ", old_lst_data_PERS_ADRESSE_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme adresses dans le composant "tags-selector-tagselect"
            # dans le fichier "PERS_ADRESSE_modifier_tags_dropbox.html"
            new_lst_str_PERS_ADRESSE = request.form.getlist('name_select_tags')
            print("new_lst_str_PERS_ADRESSE ", new_lst_str_PERS_ADRESSE)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_PERS_ADRESSE_old = list(map(int, new_lst_str_PERS_ADRESSE))
            print("new_lst_PERS_ADRESSE ", new_lst_int_PERS_ADRESSE_old, "type new_lst_PERS_ADRESSE ",
                  type(new_lst_int_PERS_ADRESSE_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_Adresse" qui doivent être effacés de la table intermédiaire "t_PERS_ADRESSE".
            lst_diff_adresses_delete_b = list(
                set(old_lst_data_PERS_ADRESSE_attribues) - set(new_lst_int_PERS_ADRESSE_old))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_adresses_delete_b ", lst_diff_adresses_delete_b)

            # OM 2020.04.29 Une liste de "id_Adresse" qui doivent être ajoutés à la BD
            lst_diff_adresses_insert_a = list(
                set(new_lst_int_PERS_ADRESSE_old) - set(old_lst_data_PERS_ADRESSE_attribues))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_adresses_insert_a ", lst_diff_adresses_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresses = GestionPersAdresses()

            # Pour le film sélectionné, parcourir la liste des adresses à INSÉRER dans la "t_PERS_ADRESSE".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_Adresse_ins in lst_diff_adresses_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                # et "id_Adresse_ins" (l'id du adresse dans la liste) associé à une variable.
                valeurs_film_sel_adresse_sel_dictionnaire = {"value_fk_pers": id_Personne_selected,
                                                           "value_fk_adresse": id_Adresse_ins}
                # Insérer une association entre un(des) adresse(s) et le film sélectionner.
                obj_actions_adresses.PERS_ADRESSE_add(valeurs_film_sel_adresse_sel_dictionnaire)

            # Pour le film sélectionné, parcourir la liste des adresses à EFFACER dans la "t_PERS_ADRESSE".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_Adresse_del in lst_diff_adresses_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                # et "id_Adresse_del" (l'id du adresse dans la liste) associé à une variable.
                valeurs_film_sel_adresse_sel_dictionnaire = {"value_fk_pers": id_Personne_selected,
                                                           "value_fk_adresse": id_Adresse_del}
                # Effacer une association entre un(des) adresse(s) et le film sélectionner.
                obj_actions_adresses.PERS_ADRESSE_delete(valeurs_film_sel_adresse_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe gestionadresses()
            # Fichier data_gestion_adresses.py
            # Afficher seulement le film dont les adresses sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_PERS_ADRESSE_afficher_concat = obj_actions_adresses.PERS_ADRESSE_afficher_data_concat(id_Personne_selected)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data adresses", data_PERS_ADRESSE_afficher_concat, "type ", type(data_PERS_ADRESSE_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_PERS_ADRESSE_afficher_concat == None:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_PERS_ADRESSE" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données adresses affichées dans PersAdresse!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_PERS_ADRESSE",
    # on affiche les personnes et le(urs) adresse(s) associé(s).
    return render_template("PERS_ADRESSE/PERS_ADRESSE_afficher.html",
                           data=data_PERS_ADRESSE_afficher_concat)
