# routes_gestion_adresses.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les adresses.

import pymysql
from flask import render_template, flash, redirect, url_for, request
from APP_ENTREPRISE import obj_mon_application
from APP_ENTREPRISE.ADRESSES.data_gestion_adresses import gestionadresses
from APP_ENTREPRISE.DATABASE.erreurs import *

import re





# OM 2020.04.16 Afficher les adresses
# Pour la tester http://127.0.0.1:5005/adresses_afficher
@obj_mon_application.route("/adresses_afficher")
def adresses_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_films = gestionadresses()
            # Récupère les données grâce à une requête MySql définie dans la classe gestionadresses()
            # Fichier data_gestion_adresses.py
            data_adresses = obj_actions_films.adresses_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data adresses", data_adresses, "type ", type(data_adresses))
            # Différencier les messages si la table est vide.
            if data_adresses:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données adresses affichées !!", "success")
            else:
                flash("""La table "t_films" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}","danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("adresses/adresses_afficher.html", data=data_adresses)


# OM 2020.04.06 Pour une simple démo. On insère deux fois des valeurs dans la table adresses
# Une fois de manière fixe, vous devez changer les valeurs pour voir le résultat dans la table "t_films"
# La 2ème il faut entrer la valeur du titre du film par le clavier, il ne doit pas être vide.
# Pour les autres valeurs elles doivent être changées ci-dessous.
# Une des valeurs est "None" ce qui en MySql donne "NULL" pour l'attribut "t_films.cover_link_film"
# Pour la tester http://127.0.0.1:5005/films_add
@obj_mon_application.route("/adresses_add", methods=['GET', 'POST'])
def adresses_add ():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresses = gestionadresses()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "adresses_add.html"
            rue_adresse = request.form['rue_adresse_html']
            Npa_adresse = request.form['Npa_adresse_html']
            VilleAdresse = request.form['VilleAdresse_html']







            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^(\d)",
                            Npa_adresse):
                # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")
                # On doit afficher à nouveau le formulaire "adresses_add.html" à cause des erreurs de "claviotage"
                return render_template("adresses/adresses_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_RueAdresse": rue_adresse,
                                                   "value_NpaAdresse": Npa_adresse,
                                                   "value_VilleAdresse": VilleAdresse}

                obj_actions_adresses.add_adresse_data(valeurs_insertion_dictionnaire)

                # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")
                # On va interpréter la "route" 'adresses_afficher', car l'utilisateur
                # doit voir le nouveau adresse qu'il vient d'insérer. Et on l'affiche de manière
                # à voir le dernier élément inséré.
                return redirect(url_for('adresses_afficher', order_by = 'DESC', id_Adresse_sel=0))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("adresses/adresses_add.html")

@obj_mon_application.route('/adresses_edit', methods=['POST', 'GET'])
def adresses_edit():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
     # d'une seule ligne choisie par le bouton "edit" dans le formulaire "adresses_afficher.html"
    if request.method == 'GET':
        try:
            # Récupère la valeur de "id_Adresse" du formulaire html "adresses_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_Adresse"
            # grâce à la variable "id_Adresse_edit_html"
            # <a href="{{ url_for('adresses_edit', id_Adresse_edit_html=row.id_Adresse) }}">Edit</
            #id_Adresse_edit = request.values['id_Adresse_edit_html']
            #id_Adresse_edit = request.values['id_Adresse_edit_html']
            id_Adresse_edit = request.values['id_Adresse_edit_html']
            print("okok")


            # Pour afficher dans la console la valeur de "id_Adresse_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_Adresse_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_Adresse": id_Adresse_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresses = gestionadresses()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_Adresse = obj_actions_adresses.edit_adresse_data(valeur_select_dictionnaire)
            print("dataIdadresse ", data_id_Adresse, "type ", type(data_id_Adresse))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le adresse d'un film !!!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("adresses/adresses_edit.html", data=data_id_Adresse)

    # ---------------------------------------------------------------------------------------------------
    # OM 2020.04.07 Définition d'une "route" /adresses_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
    # au navigateur par la méthode "render_template".
    # On change la valeur d'un adresse de adresses par la commande MySql "UPDATE"
    # ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/adresses_update', methods=['POST', 'GET'])
def adresses_update():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "adresses_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du adresse alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ", request.values)

            # Récupère la valeur de "id_Adresse" du formulaire html "adresses_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_Adresse"
            # grâce à la variable "id_Adresse_edit_html"
            # <a href="{{ url_for('adresses_edit', id_Adresse_edit_html=row.id_Adresse) }}">Edit</a>
            id_Adresse_edit = request.values['id_Adresse_edit_html']

            # Récupère le contenu du champ "RueAdresseonne" dans le formulaire HTML "adressesEdit.html"
            RueAdresse = request.values['name_edit_RueAdresse_html']
            NpaAdresse = request.values['name_edit_NpaAdresse_html']
            VilleAdresse = request.values['name_edit_VilleAdresse_html']



            valeur_edit_list = [{'id_Adresse': id_Adresse_edit, 'RueAdresse': RueAdresse,'NpaAdresse':NpaAdresse,'VilleAdresse':VilleAdresse}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^(\d)",
                            NpaAdresse):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "RueAdresseonne" dans le formulaire HTML "adressesEdit.html"
                # name_adresse = request.values['name_edit_RueAdresseonne_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")

                # On doit afficher à nouveau le formulaire "adresses_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "adresses_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_Adresse': 13, 'RueAdresseonne': 'philosophique'}]
                valeur_edit_list = [{'id_Adresse': id_Adresse_edit, 'RueAdresse': RueAdresse,'NpaAdresse':NpaAdresse,'VilleAdresse':VilleAdresse}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "adresses_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('adresses/adresses_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_Adresse": id_Adresse_edit, "value_RueAdresse": RueAdresse,"value_NpaAdresse":NpaAdresse,"value_VilleAdresse":VilleAdresse}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_adresses = gestionadresses()

                # La commande MySql est envoyée à la BD
                data_id_Adresse = obj_actions_adresses.update_adresse_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataIdadresse ", data_id_Adresse, "type ", type(data_id_Adresse))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Valeur adresse modifiée. ", "success")
                # On affiche les adresses avec celui qui vient d'être edité en tête de liste. (DESC)
                return redirect(url_for('adresses_afficher', order_by="ASC", id_Adresse_sel=id_Adresse_edit))

        except (Exception,
                # pymysql.err.OperationalError,
                # pymysql.ProgrammingError,
                # pymysql.InternalError,
                # pymysql.IntegrityError,
                TypeError) as erreur:
            print(erreur.args[0])
            flash(f"problème adresses ____lllupdate{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_RueAdresseonne_html" alors on renvoie le formulaire "EDIT"
    return render_template('adresses/adresses_edit.html', data=valeur_edit_list)

@obj_mon_application.route('/adresses_select_delete', methods=['POST', 'GET'])
def adresses_select_delete ():
    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obg_actions_adresses = gestionadresses()
            # OM 2019.04.04 Récupère la valeur de "idmailDeleteHTML" du formulaire html "mailsDelete.html"
            id_Adresse_delete = request.args.get('id_Adresse_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_Adresse": id_Adresse_delete}

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_Adresse = obg_actions_adresses.delete_select_adresse_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur adresses_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur adresses_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('adresses/adresses_delete.html', data=data_id_Adresse)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /mailsUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un mail, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/adresses_delete', methods=['POST', 'GET'])
def adresses_delete ():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obg_actions_adresses = gestionadresses()
            # OM 2019.04.02 Récupère la valeur de "id_mail" du formulaire html "mailsAfficher.html"
            id_Adresse_delete = request.form['id_Adresse_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_Adresse": id_Adresse_delete}

            data_adresses = obg_actions_adresses.delete_adresse_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des mails des adresses
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les mails
            return redirect(url_for('adresses_afficher',order_by="ASC",id_Adresse_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "mail" de adresses qui est associé dans "t_genres_films".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des adresses !', "warning")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce mail est associé à des adresses dans la t_genres_films !!! : {erreur}")
                # Afficher la liste des mails des adresses
                return redirect(url_for('adresses_afficher', order_by="ASC", id_Adresse_sel=0))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur adresses_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur adresses_delete {erreur.args[0], erreur.args[1]}", "danger")

            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('adresses/adresses_afficher.html', data=data_adresses)
