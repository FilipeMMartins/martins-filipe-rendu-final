# routes_gestion_pers_mail.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les personnes et les mails.

from flask import render_template, request, flash, session
from APP_ENTREPRISE import obj_mon_application
from APP_ENTREPRISE.MAILS.data_gestion_mails import Gestionmails
from APP_ENTREPRISE.PERS_MAIL.data_gestion_pers_mail  import GestionmailsFilms


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /PERS_MAIL_afficher_concat
# Récupère la liste de tous les personnes et de tous les mails associés aux personnes.
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/pers_mail_afficher_concat/<int:id_Personne_sel>", methods=['GET', 'POST'])
def pers_mail_afficher_concat (id_Personne_sel):
    print("id_Personne_sel ", id_Personne_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionmailsFilms()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmails()
            # Fichier data_gestion_mails.py
            data_PERS_MAIL_afficher_concat = obj_actions_mails.PERS_MAIL_afficher_data_concat(id_Personne_sel)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data mails", data_PERS_MAIL_afficher_concat, "type ", type(data_PERS_MAIL_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_PERS_MAIL_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mails affichés dans mailsFilms!!", "success")
            else:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_PERS_MAIL" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("PERS_MAIL/PERS_MAIL_afficher.html",
                           data=data_PERS_MAIL_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_mail_film_selected
# Récupère la liste de tous les mails du film sélectionné.
# Nécessaire pour afficher tous les "TAGS" des mails, ainsi l'utilisateur voit les mails à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_edit_mail_film_selected", methods=['GET', 'POST'])
def gf_edit_mail_film_selected ():
    if request.method == "GET":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = Gestionmails()
            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmails()
            # Fichier data_gestion_mails.py
            # Pour savoir si la table "t_mails" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(PERS_MAIL_modifier_tags_dropbox.html)
            data_mails_all = obj_actions_mails.mails_afficher_data('ASC', 0)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_mails = GestionmailsFilms()

            # OM 2020.04.21 Récupère la valeur de "id_Personne" du formulaire html "PERS_MAIL_afficher.html"
            # l'utilisateur clique sur le lien "Modifier mails de ce film" et on récupère la valeur de "id_Personne" grâce à la variable "id_Personne_mails_edit_html"
            # <a href="{{ url_for('gf_edit_mail_film_selected', id_Personne_mails_edit_html=row.id_Personne) }}">Modifier les mails de ce film</a>
            id_Personne_mails_edit = request.values['id_Personne_mails_edit_html']

            # OM 2020.04.21 Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_Personne_mails_edit'] = id_Personne_mails_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_Personne_selected_dictionnaire = {"value_id_Personne_selected": id_Personne_mails_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la classe GestionmailsFilms()
            # 1) Sélection du film choisi
            # 2) Sélection des mails "déjà" attribués pour le film.
            # 3) Sélection des mails "pas encore" attribués pour le film choisi.
            # Fichier data_gestion_pers_mail.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "PERS_MAIL_afficher_data"
            data_mail_film_selected, data_PERS_MAIL_non_attribues, data_PERS_MAIL_attribues = \
                obj_actions_mails.PERS_MAIL_afficher_data(valeur_id_Personne_selected_dictionnaire)

            lst_data_film_selected = [item['id_Personne'] for item in data_mail_film_selected]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_film_selected  ", lst_data_film_selected,
                  type(lst_data_film_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les mails qui ne sont pas encore sélectionnés.
            lst_data_PERS_MAIL_non_attribues = [item['id_mail'] for item in data_PERS_MAIL_non_attribues]
            session['session_lst_data_PERS_MAIL_non_attribues'] = lst_data_PERS_MAIL_non_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_PERS_MAIL_non_attribues  ", lst_data_PERS_MAIL_non_attribues,
                  type(lst_data_PERS_MAIL_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les mails qui sont déjà sélectionnés.
            lst_data_PERS_MAIL_old_attribues = [item['id_mail'] for item in data_PERS_MAIL_attribues]
            session['session_lst_data_PERS_MAIL_old_attribues'] = lst_data_PERS_MAIL_old_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_PERS_MAIL_old_attribues  ", lst_data_PERS_MAIL_old_attribues,
                  type(lst_data_PERS_MAIL_old_attribues))

            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data data_mail_film_selected", data_mail_film_selected, "type ", type(data_mail_film_selected))
            print(" data data_PERS_MAIL_non_attribues ", data_PERS_MAIL_non_attribues, "type ",
                  type(data_PERS_MAIL_non_attribues))
            print(" data_PERS_MAIL_attribues ", data_PERS_MAIL_attribues, "type ",
                  type(data_PERS_MAIL_attribues))

            # Extrait les valeurs contenues dans la table "t_mails", colonne "NomMail"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_mail
            lst_data_PERS_MAIL_non_attribues = [item['NomMail'] for item in data_PERS_MAIL_non_attribues]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_all_mails gf_edit_mail_film_selected ", lst_data_PERS_MAIL_non_attribues,
                  type(lst_data_PERS_MAIL_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_film_selected == [None]:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_PERS_MAIL" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mails affichées dans mailsFilms!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("PERS_MAIL/PERS_MAIL_modifier_tags_dropbox.html",
                           data_mails=data_mails_all,
                           data_film_selected=data_mail_film_selected,
                           data_mails_attribues=data_PERS_MAIL_attribues,
                           data_mails_non_attribues=data_PERS_MAIL_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_mail_film_selected
# Récupère la liste de tous les mails du film sélectionné.
# Nécessaire pour afficher tous les "TAGS" des mails, ainsi l'utilisateur voit les mails à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_update_mail_film_selected", methods=['GET', 'POST'])
def gf_update_mail_film_selected ():
    if request.method == "POST":
        try:
            # Récupère l'id du film sélectionné
            id_Personne_selected = session['session_id_Personne_mails_edit']
            print("session['session_id_Personne_mails_edit'] ", session['session_id_Personne_mails_edit'])

            # Récupère la liste des mails qui ne sont pas associés au film sélectionné.
            old_lst_data_PERS_MAIL_non_attribues = session['session_lst_data_PERS_MAIL_non_attribues']
            print("old_lst_data_PERS_MAIL_non_attribues ", old_lst_data_PERS_MAIL_non_attribues)

            # Récupère la liste des mails qui sont associés au film sélectionné.
            old_lst_data_PERS_MAIL_attribues = session['session_lst_data_PERS_MAIL_old_attribues']
            print("old_lst_data_PERS_MAIL_old_attribues ", old_lst_data_PERS_MAIL_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme mails dans le composant "tags-selector-tagselect"
            # dans le fichier "PERS_MAIL_modifier_tags_dropbox.html"
            new_lst_str_PERS_MAIL = request.form.getlist('name_select_tags')
            print("new_lst_str_PERS_MAIL ", new_lst_str_PERS_MAIL)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_PERS_MAIL_old = list(map(int, new_lst_str_PERS_MAIL))
            print("new_lst_PERS_MAIL ", new_lst_int_PERS_MAIL_old, "type new_lst_PERS_MAIL ",
                  type(new_lst_int_PERS_MAIL_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_mail" qui doivent être effacés de la table intermédiaire "t_PERS_MAIL".
            lst_diff_mails_delete_b = list(
                set(old_lst_data_PERS_MAIL_attribues) - set(new_lst_int_PERS_MAIL_old))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_mails_delete_b ", lst_diff_mails_delete_b)

            # OM 2020.04.29 Une liste de "id_mail" qui doivent être ajoutés à la BD
            lst_diff_mails_insert_a = list(
                set(new_lst_int_PERS_MAIL_old) - set(old_lst_data_PERS_MAIL_attribues))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_mails_insert_a ", lst_diff_mails_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionmailsFilms()

            # Pour le film sélectionné, parcourir la liste des mails à INSÉRER dans la "t_PERS_MAIL".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_mail_ins in lst_diff_mails_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                # et "id_mail_ins" (l'id du mail dans la liste) associé à une variable.
                valeurs_film_sel_mail_sel_dictionnaire = {"value_fk_pers": id_Personne_selected,
                                                           "value_fk_mail": id_mail_ins}
                # Insérer une association entre un(des) mail(s) et le film sélectionner.
                obj_actions_mails.PERS_MAIL_add(valeurs_film_sel_mail_sel_dictionnaire)

            # Pour le film sélectionné, parcourir la liste des mails à EFFACER dans la "t_PERS_MAIL".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_mail_del in lst_diff_mails_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                # et "id_mail_del" (l'id du mail dans la liste) associé à une variable.
                valeurs_film_sel_mail_sel_dictionnaire = {"value_fk_pers": id_Personne_selected,
                                                           "value_fk_mail": id_mail_del}
                # Effacer une association entre un(des) mail(s) et le film sélectionner.
                obj_actions_mails.PERS_MAIL_delete(valeurs_film_sel_mail_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe Gestionmails()
            # Fichier data_gestion_mails.py
            # Afficher seulement le film dont les mails sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_PERS_MAIL_afficher_concat = obj_actions_mails.PERS_MAIL_afficher_data_concat(id_Personne_selected)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data mails", data_PERS_MAIL_afficher_concat, "type ", type(data_PERS_MAIL_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_PERS_MAIL_afficher_concat == None:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_PERS_MAIL" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données mails affichées dans mailsFilms!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_PERS_MAIL",
    # on affiche les personnes et le(urs) mail(s) associé(s).
    return render_template("PERS_MAIL/PERS_MAIL_afficher.html",
                           data=data_PERS_MAIL_afficher_concat)
